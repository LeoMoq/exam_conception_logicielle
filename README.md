# EXAMEN CONCEPTION LOGICIELLE

Elève : MOQUAY Léo
Examen final

## Comment lancer l'application :

### Partie Serveur

Dans un premier terminal, allez dans la partie serveur (commande cd serveur) et suivez les étapes suivantes :

1ère étape : créer un environnement virtuel
`python -m venv env`

2ème étape : lancer l'environnement virtuel
`"env/Scripts/activate"`

3ème étape : télécharger les dependances
`pip install -r requirements.txt`

4ème étape : lancer le webservice
`uvicorn main:app --reload`

5ème étape : acceder au [WebService](http://127.0.0.1:8000/)

### Partie Client

Dans un second terminal, allez dans la partie client (cd client) et suivez les étapes suivantes :

1ère étape : télécharger les dépendances
`pip install -r requirements.txt`

2ème étape : Lancer le client
`python main.py`
